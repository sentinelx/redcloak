#!/bin/bash
#took the original code from https://github.com/coffeegist/now-you-see-me
#with cerbot-auto depreciated I needed to rework this automated solution so here you have RedCloak!
#dirty but it works!

NORMAL=`echo "\033[m"`
BRED=`printf "\e[1;31m"`
BGREEN=`printf "\e[1;32m"`
BYELLOW=`printf "\e[1;33m"`
COLUMNS=12

error_exit() {
  echo -e "\n$1\n" 1>&2
  exit 1
}


redcloack_error() {
  printf "\n${BRED}[!] $1${NORMAL}\n"
}

progress_action(){
    printf "\n$BGREEN}[+]${NORMAL} $1\n"

}

check_error(){
    if [ $? -ne 0 ]; then
    redcloack_error " Error"

    error_exit "Exiting"

        fi
}
dependencies_install(){

    progress_action "Installing required packages"
    apt update -y
    apt install -y vim less

    progress_action " Installing net tools"
    apt install -y net-tools screen dnsutils curl wget inetutils-pinginsta

    progress_action "Installing nginx and git oh and snap"
    apt install nginx git -y
    snap install core 
    snap refresh core

    #snap certbot keeps everything updated
   progress_action " installing cerbot via snap"
   apt remove certbot
   snap install --classic certbot
   
}
redcloak_status() {
  printf "\n************************ Processes ************************\n"
  ps aux | grep -E 'nginx' | grep -v grep

  printf "\n************************* Network *************************\n"
  netstat -tulpn | grep -E 'nginx'
}

redcloak(){
    CONF_DEST="/etc/nginx/sites-enabled/default"
    progress_action "cue the red lights and red team swag: "
    read -r -p "what is the domain name: " domain_name
    read -r -p "what is the Ip:PORT or HOSTNAME:PORT : " c2_address
    #
      #system link
    ls -s /snap/bin/certbot /usr/bin/certbot
    #get certs and cerbot will automatically edit your nginx config
    cp ./default.conf $CONF_DEST

    sed -i.bak "s/<DOMAIN_NAME>/$domain_name/" $CONF_DEST
    rm $CONF_DEST.bak

    sed -i.bak "s/<C2_SERVER>/$c2_address/" $CONF_DEST
    rm $CONF_DEST.bak
    check_error

    SSL_SRC='/etc/letsencrypt/live/$domain_name'
    progress_action "Getting Certs "
    certbot --agree-tos --non-interactive --nginx --webroot-path /var/www/html -d $domain_name --register-unsafely-without-email
    check_error

    #installing certs and modifying config based on NYSM
    sed -i.bak "s/^#redcloak#//g" $CONF_DEST
    rm $CONF_DEST.bak
    check_error

    progress_action "restarting nginx"
    systemctl restart nginx.service
    check_error


  progress_action "Cloak Up!"
}

if [ "$#" -ne 2 ]; then
  PS3="
  RedCloak - Select an Option:  "

  finshed=0
  while (( !finished )); do
    printf "\n"
    options=("Setup Nginx Redirector" "Check Status" "Quit")
    select opt in "${options[@]}"
    do
      case $opt in
        "Setup Nginx Redirector")
          dependencies_install
          redcloak
          break;
          ;;
        "Check Status")
          redcloak_status
          break;
          ;;
        "Quit")
          finished=1
          break;
          ;;
        *) progress_action "invalid option" ;;
      esac
    done
  done
else
  redcloak_setup $1 $2
fi
